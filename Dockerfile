FROM python:3.9.7-buster

RUN apt-get update && \
    apt-get install -y curl git wget bash sshpass jq zip unzip gzip ccze tree && \
    pip3 install anybadge && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*  && \
    rm -f /var/cache/apt/archives/*.deb  && \
    rm -f var/cache/apt/archives/partial/*.deb  && \
    rm -f /var/cache/apt/*.bin

